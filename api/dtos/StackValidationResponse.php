<?php

namespace api\dtos;

class StackValidationResponse
{
    /** @var string */
    public string $Validation;
}
